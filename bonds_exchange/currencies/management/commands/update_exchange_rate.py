from django.core.management.base import BaseCommand
from currencies.models import USD_MXN
from datetime import datetime, timedelta
import requests
import json

class Command(BaseCommand):
    help = 'Update exchange rate'

    def handle(self, *args, **options):
        date = datetime.now().strftime('%Y-%m-%d')
        yesterday = (datetime.now()-timedelta(days=1)).strftime('%Y-%m-%d')
        url = f"https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF43718/datos/{yesterday}/{date}?token=57cae14d02f727c27108317da12b7f96c4cba8abb52da44be45f5c0abbf4c479"
        r = requests.get(url)
        if r.status_code==200:
            r = r.json()
            rate = float(r.get('bmx').get('series')[0].get('datos')[0].get('dato'))
            USD_MXN.objects.create(rate=rate)
            self.stdout.write(self.style.SUCCESS("Success"))

        else:
            self.stdout.write(self.style.ERROR("Error"))