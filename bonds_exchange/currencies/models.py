from django.db import models
from django.utils.translation import gettext_lazy as _
from datetime import datetime
import requests

class USD_MXN(models.Model):
    date = models.DateField(auto_now_add=True)
    rate = models.DecimalField(max_digits=15, decimal_places=6)

    class Meta:
        ordering = ['-date']