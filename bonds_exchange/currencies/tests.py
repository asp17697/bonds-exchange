from io import StringIO
from django.core.management import call_command
from django.test import TestCase


class ExchangeRateTest(TestCase):
    def test_command_exchange(self):
        out = StringIO()
        call_command('update_exchange_rate', stdout=out)
        self.assertIn('Success', out.getvalue())