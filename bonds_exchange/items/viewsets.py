from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from .serializers import ItemSellSerializer, ItemBuySerializerUSD, ItemBuySerializer
from .models import Item
from rest_framework.decorators import action
from rest_framework.response import Response
from datetime import datetime

class ItemSellViewSet(viewsets.ModelViewSet):
    serializer_class = ItemSellSerializer
    filter_backends = [DjangoFilterBackend]

    def get_permissions(self):
        return (IsAuthenticated(),)

    def get_queryset(self):
        queryset = Item.objects.filter(seller=self.request.user)
        return queryset


class ItemBuyViewSet(viewsets.ModelViewSet):
    serializer_class = ItemBuySerializer
    filter_backends = [DjangoFilterBackend]

    def get_permissions(self):
        return (IsAuthenticated(),)

    def get_serializer_class(self, *args, **kwargs):
        if self.request.query_params.get("currency", None) == 'USD':
            return ItemBuySerializerUSD
        return ItemBuySerializer

    def get_queryset(self):
        if self.request.query_params.get("own", None):
            queryset = Item.objects.filter(buyer=self.request.user)
        queryset = Item.objects.filter(buyer__isnull=True)
        return queryset

    @action(detail=True, methods=['get'], url_path='buy_bond', permission_classes=[IsAuthenticated], serializer_class=ItemBuySerializer)
    def buy_bund(self, request, *args, **kwargs):
        object = self.get_object()
        object.buyer = request.user
        object.purchase_date = datetime.today()
        object.save()

        return Response("ok")
