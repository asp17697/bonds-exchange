from rest_framework import serializers
from .models import Item

class ItemSellSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        validated_data['seller'] = self.context['request'].user
        return super(ItemSellSerializer, self).create(validated_data)

    class Meta:
        model = Item
        fields = ['pk', 'bond_name', 'amount', 'price', 'purchase_date', 'publish_date', 'sold_out']
        read_only_fields = ['pk', 'purchase_date', 'publish_date', 'sold_out']

    

class ItemBuySerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ['pk', 'bond_name', 'amount', 'price', 'publish_date', 'sold_out']
        read_only_fields = ['pk', 'publish_date', 'sold_out']

class ItemBuySerializerUSD(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ['pk', 'bond_name', 'amount', 'price_usd', 'publish_date', 'sold_out']
        read_only_fields = ['pk', 'publish_date', 'sold_out', 'price_usd']