from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator, MinLengthValidator
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from currencies.models import USD_MXN

class Item(models.Model):
    seller = models.ForeignKey(User, on_delete=models.RESTRICT, verbose_name=_("seller"), related_name='items_on_sale')
    bond_name = models.CharField(_("bond name"), validators=[MinLengthValidator(3)], max_length=40)
    amount = models.IntegerField(_("amount"), validators=[MinValueValidator(1), MaxValueValidator(10_000)])
    price = models.FloatField(_("price"), validators=[MinValueValidator(0.0), MaxValueValidator(1_100_000_000.0)])
    publish_date = models.DateTimeField(_("publish date"), auto_now_add=True)
    buyer = models.ForeignKey(User, on_delete=models.SET_NULL, verbose_name=_("buyer"), null=True, blank=True, related_name='items_buyed')
    purchase_date = models.DateTimeField(_("purchase date"), null=True, blank=True)

    @property
    def sold_out(self):
        return self.buyer is not None

    @property
    def price_usd(self):
        rate = USD_MXN.objects.all()
        if rate.exists():
            rate = rate.first().rate
            return self.price*rate
        return None
