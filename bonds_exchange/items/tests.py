from ast import Delete
from http.client import ResponseNotReady
from django.test import TestCase, TransactionTestCase
from rest_framework.test import APITransactionTestCase
from rest_framework import status
from django.contrib.auth.models import User
from .models import Item

class SellAPITests(APITransactionTestCase):
    def setUp(self) -> None:
        User.objects.create_user("test_user_1", "random_password1.")

    def test_unauthorized(self):
        response = self.client.post('/api/v1/bonds/sell/')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_success_methods(self):
        #auth
        self.client.force_authenticate(User.objects.all().first())
        #test post
        params = {
            "bond_name": "trial",
            "amount": 55,
            "price": 120.32
        }
        response = self.client.post('/api/v1/bonds/sell/', params)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Item.objects.all().count(), 1)

        response = self.client.get('/api/v1/bonds/sell/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_wrong_data(self):
        #auth
        self.client.force_authenticate(User.objects.all().first())
        #test post
        params = {
            "bond_name": "trial",
            "amount": 55.01,
            "price": 120.32
        }
        response = self.client.post('/api/v1/bonds/sell/', params)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Item.objects.all().count(), 0)

        params = {
            "bond_name": "trial",
            "amount": 55.01,
            "price": 1_100_000_005.0
        }
        response = self.client.post('/api/v1/bonds/sell/', params)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Item.objects.all().count(), 0)

class BuyAPITests(APITransactionTestCase):
    def setUp(self) -> None:
        user = User.objects.create_user("test_user_1", "random_password1.")
        Item.objects.create(
            seller = user,
            bond_name = "trial",
            amount = 5503,
            price= 554.26
            )

    def test_unauthorized(self):
        response = self.client.post('/api/v1/bonds/buy/')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list(self):
        self.client.force_authenticate(User.objects.all().first())

        #test list usd
        response = self.client.get('/api/v1/bonds/buy?currency=USD')
        self.assertIsNot(response.status_code, status.HTTP_400_BAD_REQUEST)
        
        #test list mxn
        response = self.client.get('/api/v1/bonds/buy')
        self.assertIsNot(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_buy(self):
        self.client.force_authenticate(User.objects.all().first())
        #test post

        response = self.client.get('/api/v1/bonds/buy/1/buy_bond/')
        #self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Item.objects.filter(buyer__isnull=False).count(), 1)