from rest_framework_extensions.routers import ExtendedDefaultRouter
from django.urls import path, include
from .viewsets import ItemSellViewSet, ItemBuyViewSet

router = ExtendedDefaultRouter()
router.register(r'sell', ItemSellViewSet, basename='sell')
router.register(r'buy', ItemBuyViewSet, basename='buy')

urlpatterns = [
    path('', include(router.urls))
]