pre-requistes:
    - Docker
    - Folder named "postgres-data" on home directory for postgres volume handle

commands to run:
    - docker-compose -f docker-compose.yml build
    - docker-compose up
    - docker-compose -f docker-compose.yml run --rm web python3 manage.py makemigrations
    - docker-compose -f docker-compose.yml run --rm web python3 manage.py migrate
    - docker-compose -f docker-compose.yml run --rm web python3 manage.py test
    - docker-compose -f docker-compose.yml run --rm web python3 manage.py createsuperuser

documentation:
    - swagger: api/v1/schema/swagger-ui/
    - redoc: api/v1/schema/redoc/