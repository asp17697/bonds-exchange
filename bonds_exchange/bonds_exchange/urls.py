from django.contrib import admin
from django.urls import path, re_path
from django.urls.conf import include
from rest_framework.authtoken import views
from drf_spectacular.views import SpectacularAPIView, SpectacularRedocView, SpectacularSwaggerView

v1_urlpatterns = [
    path('api/v1/auth/token-auth/', views.obtain_auth_token),

    re_path(r'^api/v1/bonds/', include('items.urls'), name='items')
]


docs_urlpatterns = [
    path('api/v1/schema/', SpectacularAPIView.as_view(), name='schema'),
    path('api/v1/schema/swagger-ui/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
    path('api/v1/schema/redoc/', SpectacularRedocView.as_view(url_name='schema'), name='redoc'),
]

urlpatterns = [
    path('admin/', admin.site.urls)
]

urlpatterns += v1_urlpatterns
urlpatterns += docs_urlpatterns

handler400 = 'rest_framework.exceptions.bad_request'
